package pk.labs.LabD.common;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import org.osgi.service.component.ComponentContext;
import pk.labs.LabD.contracts.Animal;
import pk.labs.LabD.contracts.Logger;

public class InstanceAnimal implements Animal {

    public void bindLogger(Logger logger) {
        this.logger = logger;
    }

    public void activate(ComponentContext compCtx) {
        //this.status = compCtx.getProperties().get("status").toString();
        name = compCtx.getProperties().get("name").toString();
        species = compCtx.getProperties().get("species").toString();
        logger.log(this, species + " biegnie");
        System.out.println("aktywne");
    }

    public void deactivate(ComponentContext compCtx) {
        name = compCtx.getProperties().get("name").toString();
        species = compCtx.getProperties().get("species").toString();
        logger.log(this, species + " odbiega");
        System.out.println("nie-aktywne");
    }

    private Logger logger;
    private String species;
    private String name;
    private String status;
    private PropertyChangeSupport listeners = new PropertyChangeSupport(this);

    @Override
    public String getSpecies() {
        return species;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getStatus() {
        return status;
    }

    @Override
    public void setStatus(String status) {
        String oldValue = this.status;
        this.status = status;
        listeners.firePropertyChange(new PropertyChangeEvent(this, "status", oldValue, status));
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        listeners.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        listeners.removePropertyChangeListener(listener);
    }
}
