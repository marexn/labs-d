/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.LabD.actions.internal;

import pk.labs.LabD.contracts.Animal;
import pk.labs.LabD.contracts.AnimalAction;
/**
 *
 * @author Marcin
 */
public class Action3 implements AnimalAction {
    
    public String toString()
    {
        return "Akcja3";
    }
    
    @Override
    public boolean execute(Animal animal) {
        animal.setStatus("Akcja3");
        return true;
    }
}
