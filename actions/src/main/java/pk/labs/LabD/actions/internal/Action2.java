/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.LabD.actions.internal;

import pk.labs.LabD.contracts.Animal;
import pk.labs.LabD.contracts.AnimalAction;
/**
 *
 * @author Marcin
 */
public class Action2 implements AnimalAction {
    
    public String toString()
    {
        return "Akcja2";
    }
    
    @Override
    public boolean execute(Animal animal) {
        animal.setStatus("Akcja2");
        return true;
    }
}
